# razor-pages

Test project to try a razor website on gitlabpages

Based on : https://stackoverflow.com/questions/70213662/how-to-deploy-blazor-webassembly-as-static-site-in-gitlab-pages

## Getting started

Install .NET 8 SDK

Create Repo & Clone

## Create the solution with web assembly Blazor project

```
dotnet new gitignore
dotnet new blazorwasm --name Sasw.Editor.Web --output src/Sasw.Editor.Web --no-https
dotnet new sln
dotnet sln add src/Sasw.Editor.Web
```

## Compile and run it

```
dotnet build
dotnet run --project src/Sasw.Editor.Web
```
port defined at the launchsettings.json

## Publish a distribution folder

```
dotnet publish -c Release -o publish
```
All the artifacts and files are now under publish folder. So, in theory, I can serve those things with a simple web server. I  (it requires NodeJs/npm but you can use any other web server)

## Install a basic web server tool local-web-server

```
npm install -g local-web-server
```

## Start the server

Move to publish/wwwroot (index.html location)
```
ws
```

# GitLab Pages configuration

create the file .gitlab-ci.yml with this :

```
image: mcr.microsoft.com/dotnet/sdk:8.0

variables:
  GIT_DEPTH: 1000
  PUBLISH_OUTPUT_DIR: publish

stages:
  - build
  - test
  - publish
  - deploy

build:
  stage: build
  script:
    - dotnet restore --no-cache --force
    - dotnet build --configuration Release --no-restore
  artifacts:
    paths:
    - test
    expire_in: 8 hour
  rules:
    - if: $CI_COMMIT_TAG
      when: never  
    - when: always

test:
  stage: test
  script: dotnet test --blame --configuration Release
  allow_failure: false
  rules:
    - if: $CI_COMMIT_TAG
      when: never  
    - exists:
      - test/**/*Tests.csproj

publish:
  stage: publish
  script:
    - dotnet publish -c Release -o $PUBLISH_OUTPUT_DIR
  artifacts:
    paths: 
      - $PUBLISH_OUTPUT_DIR/
    expire_in: 8 hour
  rules:
    - if: $CI_COMMIT_TAG
      when: never  
    - when: on_success

pages:
  stage: deploy
  variables:
    SED_COMMAND: 's#<base\shref="\/"\s?\/>#<base href="\/$CI_PROJECT_NAME\/" \/>#g'
  script:
    - cp -a $PUBLISH_OUTPUT_DIR/wwwroot public
    - sed -r -i "$SED_COMMAND" public/index.html
  artifacts:
    paths:
    - public
  only:
    - main
```

May need to change the sdk version on the first line to adapt to the fitted version.